import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_apache2_service(host):
    apache2 = host.service("apache2")
    assert apache2.is_running
    assert apache2.is_enabled


def test_apache2_server(host):
	assert host.socket("tcp://127.0.0.1:80").is_listening
	content = host.run("curl localhost:80")
	assert 'verivina apache wsgi.py' in content.stdout
